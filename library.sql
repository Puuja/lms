-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 07, 2020 at 07:59 PM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `library`
--

-- --------------------------------------------------------

--
-- Table structure for table `books`
--

CREATE TABLE `books` (
  `book_id` int(11) NOT NULL,
  `book_name` varchar(200) CHARACTER SET latin1 NOT NULL,
  `author` varchar(200) CHARACTER SET latin1 NOT NULL,
  `rack_number` int(50) NOT NULL,
  `status` varchar(50) CHARACTER SET latin1 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `books`
--

INSERT INTO `books` (`book_id`, `book_name`, `author`, `rack_number`, `status`) VALUES
(36, 'Business System Development Tools', 'Mohan Maharjan', 12, 'Available'),
(37, 'Java Web', 'Nischal Shakya', 19, 'Available'),
(38, 'Human Computer Interaction', 'Bhagwan Thapa', 25, 'Available'),
(39, 'Data Structures And Algorithm', 'Purnima Mulmi', 28, 'Available'),
(40, 'Computer Organisation Architecture', 'Purnima Mulmi', 6, 'Available'),
(41, 'Database management system', 'Rabin Thapa', 2, 'Available'),
(42, 'Discrete Mathematics', 'Monica Regmi', 5, 'Available'),
(43, 'Database Administration', 'Keshav Maharjan', 7, 'Available'),
(45, 'Science', 'Prativa Maharjan', 32, 'Available'),
(46, 'Social Science', 'Sulav shakya', 49, 'Available');

-- --------------------------------------------------------

--
-- Table structure for table `multiuserlogin`
--

CREATE TABLE `multiuserlogin` (
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `usertype` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `multiuserlogin`
--

INSERT INTO `multiuserlogin` (`username`, `password`, `usertype`) VALUES
('admin', 'admin', 'admin'),
('admin', 'admin', 'user'),
('admin', 'admin', 'admin'),
('user', 'user', 'user');

-- --------------------------------------------------------

--
-- Table structure for table `userbooks`
--

CREATE TABLE `userbooks` (
  `id` int(11) NOT NULL,
  `user_name` varchar(25) COLLATE utf8_bin NOT NULL,
  `book_name` varchar(200) COLLATE utf8_bin NOT NULL,
  `issue_date` date NOT NULL,
  `return_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `userbooks`
--

INSERT INTO `userbooks` (`id`, `user_name`, `book_name`, `issue_date`, `return_date`) VALUES
(30, 'Mohan Maharjan', 'Business System Development Tools', '2020-06-21', '2020-06-28'),
(32, 'anuj pradhan', 'Data Structures And Algorithm', '2020-06-25', '2020-07-02'),
(34, 'Prativa Maharjan', 'Java Web', '2020-06-25', '2020-07-02'),
(35, 'baine pradhan', 'Social Science', '2020-06-30', '2020-07-05'),
(36, 'Surya bahadur Tamang', 'Database management system', '2020-06-30', '2020-07-05'),
(37, 'Jiwan Maharjan', 'Human Computer Interaction', '2020-06-30', '2020-07-05'),
(38, 'Samanta Shrestha', 'Science', '2020-06-30', '2020-07-05'),
(40, 'baine pradhan', 'Computer Organisation Architecture', '2020-07-02', '2020-07-08'),
(41, 'Urmila dongol', 'Discrete Mathematics', '2020-07-02', '2020-07-08'),
(44, 'Mohan Maharjan', 'Business System Development Tools', '2020-08-09', '2020-08-16');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `user_name` varchar(200) COLLATE utf8_bin NOT NULL,
  `contact` int(80) NOT NULL,
  `address` varchar(200) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `user_name`, `contact`, `address`) VALUES
(31, 'Mohan Maharjan', 5512456, 'Patan'),
(32, 'Prativa Maharjan', 984567896, 'kirtipur'),
(33, 'anuj pradhan', 97464, 'yala'),
(34, 'Jiwan Maharjan', 8746494, 'samal'),
(35, 'Surya bahadur Tamang', 43466149, 'gorkha'),
(36, 'baine pradhan', 49434, 'hetauda'),
(38, 'Urmila dongol', 9746244, 'yala'),
(39, 'prajina singh', 49624, 'bhaktapur'),
(41, 'John Sena', 45678, 'us');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `books`
--
ALTER TABLE `books`
  ADD PRIMARY KEY (`book_id`);

--
-- Indexes for table `userbooks`
--
ALTER TABLE `userbooks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `books`
--
ALTER TABLE `books`
  MODIFY `book_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT for table `userbooks`
--
ALTER TABLE `userbooks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
