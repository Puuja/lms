package org.personal.LibraryManagement.dao.impl;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.personal.LibraryManagement.connectionfactory.ConnectionFactory;
import org.personal.LibraryManagement.dao.UserDao;
import org.personal.LibraryManagement.model.User;

public class UserDaoImpl implements UserDao {

    private static PreparedStatement preparedStatement;
    private static ResultSet resultSet;

    private Connection getConnection() throws ClassNotFoundException, SQLException {
        return ConnectionFactory.getConnection();
    }

    @Override
    public void save(User user) throws Exception {
        final String query = "insert into users(User_id, User_Name, Contact, Address) values(?,?,?,?)";
        preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setInt(1, user.getUserId());
        preparedStatement.setString(2, user.getUserName());
        preparedStatement.setString(3, user.getContact());
        preparedStatement.setString(4, user.getAddress());
        preparedStatement.executeUpdate();
    }

    @Override
    public void update(User user, int id) throws Exception {
        final String query = "update users set User_Name = ?, Contact = ?, Address = ? where User_Id = ?";
        preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setString(1, user.getUserName());
        preparedStatement.setString(2, user.getContact());
        preparedStatement.setString(3, user.getAddress());
        preparedStatement.setInt(4, id);
        preparedStatement.executeUpdate();
    }

    @Override
    public void remove(Integer id) throws Exception {
        final String query = "delete from users where User_Id = ?";
        preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setInt(1, id);
        preparedStatement.executeUpdate();
    }

    /* @Override
    public User findOne(Integer id) throws Exception {
    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }*/
    @Override
    public List<User> findAll() throws Exception {
        List<User> listOfUser = new ArrayList<>();
        final String query = "select * from users";
        preparedStatement = getConnection().prepareStatement(query);
        resultSet = preparedStatement.executeQuery();
        User user;
        while (resultSet.next()) {
            user = new User(resultSet.getInt("User_Id"), resultSet.getString("User_Name"), resultSet.getString("Contact"), resultSet.getString("Address"));
            listOfUser.add(user);

        }
        return listOfUser;
    }

}
