    
package org.personal.LibraryManagement.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.personal.LibraryManagement.connectionfactory.ConnectionFactory;
import org.personal.LibraryManagement.dao.BooksDao;
import org.personal.LibraryManagement.model.Books;

public class BooksDaoImpl implements BooksDao {

    private static PreparedStatement preparedStatement;

    private static ResultSet resultSet;

    private Connection getConnection() throws ClassNotFoundException, SQLException {
        return ConnectionFactory.getConnection();
    }

    @Override
    public void save(Books books) throws Exception {
        final String query = "Insert into books(Book_Id, Book_Name, Author, Rack_Number, Status) values(?,?,?,?,?)";
        preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setInt(1, books.getBookId());
        preparedStatement.setString(2, books.getBookName());
        preparedStatement.setString(3, books.getAuthor());
        preparedStatement.setInt(4, books.getRackNumber());
        preparedStatement.setString(5, books.getStatus());
        preparedStatement.executeUpdate();
    }

    @Override
    public void update(Books books, int id) throws Exception {
        final String query = "Update books set Book_Name = ?, Author = ?, Rack_Number = ?, Status = ? where Book_Id = ?";
        preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setString(1, books.getBookName());
        preparedStatement.setString(2, books.getAuthor());
        preparedStatement.setInt(3, books.getRackNumber());
        preparedStatement.setString(4, books.getStatus());
        preparedStatement.setInt(5, id);
        preparedStatement.executeUpdate();
    }

    @Override
    public void remove(Integer id) throws Exception {
        final String query = "delete from books where Book_Id = ?";
        preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setInt(1, id);
        preparedStatement.executeUpdate();

    }

    /* @Override
    public Books findOne(Integer id) throws Exception {
    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }*/
    @Override
    public List<Books> findAll() throws Exception {
        List<Books> listOfBooks = new ArrayList<>();
        preparedStatement = getConnection().prepareStatement("select * from books");
        resultSet = preparedStatement.executeQuery();
        Books books;
        while (resultSet.next()) {
            books = new Books(resultSet.getInt("Book_Id"), resultSet.getString("Book_Name"), resultSet.getString("Author"), resultSet.getInt("Rack_Number"), resultSet.getString("Status"));
            listOfBooks.add(books);
        }
        return listOfBooks;
    }

    @Override
    public void dispose() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
