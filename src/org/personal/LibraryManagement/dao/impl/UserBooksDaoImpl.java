
package org.personal.LibraryManagement.dao.impl;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.personal.LibraryManagement.connectionfactory.ConnectionFactory;
import org.personal.LibraryManagement.dao.UserBooksDao;
import org.personal.LibraryManagement.model.UserBooks;

public class UserBooksDaoImpl implements UserBooksDao {

    private static PreparedStatement preparedStatement;

    private static ResultSet resultSet;

    private Connection getConnection() throws ClassNotFoundException, SQLException {
        return ConnectionFactory.getConnection();
    }

    @Override
    public void save(UserBooks userbooks) throws Exception {
        final String query = "insert into userbooks(userbooks_id, user_name, book_name, issue_date, return_date) values(?,?,?,?,?)";
        preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setInt(1, userbooks.getId());
        preparedStatement.setString(2, userbooks.getUserName());
        preparedStatement.setString(3, userbooks.getBookName());
        preparedStatement.setString(4, userbooks.getIssueDate());
        preparedStatement.setString(5, userbooks.getReturnDate());
        preparedStatement.executeUpdate();
    }

    @Override
    public void update(UserBooks userbooks, int id) throws Exception {
        final String query = "update userbooks set userName = ?, bookName = ?, issueDate = ?, returnDate = ?, where id = ?";
        preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setString(1, userbooks.getUserName());
        preparedStatement.setString(2, userbooks.getBookName());
        preparedStatement.setString(3, userbooks.getIssueDate());
        preparedStatement.setString(4, userbooks.getReturnDate());
        preparedStatement.setInt(5, id);
        preparedStatement.executeUpdate();
    }

    @Override
    public void remove(Integer id) throws Exception {
        final String query = "delete from userbooks where id = ?";
        preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setInt(1, id);
        preparedStatement.executeUpdate();
    }

    @Override
    public List<UserBooks> findAll() throws Exception {
        List<UserBooks> listOfBooks = new ArrayList<>();
        final String query = "select * from userbooks";
        preparedStatement = getConnection().prepareStatement(query);
        resultSet = preparedStatement.executeQuery();
        while (resultSet.next()) {
            UserBooks userbooks = new UserBooks(resultSet.getInt("userbooks_id"),
                    resultSet.getString("user_name"),
                    resultSet.getString("book_name"),
                    resultSet.getString("issue_date"),
                    resultSet.getString("return_date"));
            listOfBooks.add(userbooks);
        }
        return listOfBooks;
    }

}
