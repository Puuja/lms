
package org.personal.LibraryManagement.dao;

import java.util.List;
import org.personal.LibraryManagement.model.UserBooks;

public interface UserBooksDao {

    void save(UserBooks userbooks) throws Exception;

    void update(UserBooks userbooks, int id) throws Exception;

    void remove(Integer id) throws Exception;

    List<UserBooks> findAll() throws Exception;

}
