
package org.personal.LibraryManagement.dao;

import java.util.List;
import org.personal.LibraryManagement.model.User;

public interface UserDao {
 
    void save(User user) throws Exception;
    
    void update(User user, int id) throws Exception;
    
    void remove(Integer id) throws Exception;
    
    //User findOne(Integer id) throws Exception;
    
    List<User> findAll() throws Exception;
    
}

    
    
    
    
    

