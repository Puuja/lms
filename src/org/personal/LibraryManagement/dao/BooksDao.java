
package org.personal.LibraryManagement.dao;


import java.util.List;
import org.personal.LibraryManagement.model.Books;

public interface BooksDao {

    void save(Books books) throws Exception;

    void update(Books books, int id) throws Exception;

    void remove(Integer id) throws Exception;

    //Books findOne(Integer id) throws Exception;
    List<Books> findAll() throws Exception;

    public void dispose();

}
