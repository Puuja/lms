
package org.personal.LibraryManagement.connectionfactory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionFactory {

    private static final String USER_NAME = "hr";

    private static final String PASSWORD = "hr";

    private static final String URL = "jdbc:oracle:thin:@localhost:1521:orcl";

    private ConnectionFactory() {
    }

    public static Connection getConnection() throws ClassNotFoundException, SQLException {
        Class.forName("oracle.jdbc.driver.OracleDriver");
        return DriverManager.getConnection(URL, USER_NAME, PASSWORD);
    }

}
