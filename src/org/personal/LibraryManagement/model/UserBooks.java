
package org.personal.LibraryManagement.model;


public class UserBooks {

    public static void setvisible(boolean b) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    private int id;
    private String userName;
    private String bookName;
    private String issueDate;
    private String returnDate;
    
    public UserBooks(){
        
    }
    
    public UserBooks(int id, String userName, String bookName, String issueDate, String returnDate) {
        this.id = id;
        this.userName = userName;
        this.bookName = bookName;
        this.issueDate = issueDate;
        this.returnDate = returnDate;
    }

    public UserBooks(String userName, String bookName, String issueDate, String returnDate) {
        this.userName = userName;
        this.bookName = bookName;
        this.issueDate = issueDate;
        this.returnDate = returnDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public String getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(String issueDate) {
        this.issueDate = issueDate;
    }

    public String getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(String returnDate) {
        this.returnDate = returnDate;
    }

    @Override
    public String toString() {
        return "UserBooks{" + "id=" + id + ", userName=" + userName + ", bookName=" + bookName + ", issueDate=" + issueDate + ", returnDate=" + returnDate + '}';
    }
    
    
    
}
