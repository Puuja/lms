
package org.personal.LibraryManagement.model;


public class Books {
    private int bookId;
    private String bookName;
    private String Author;
    private int rackNumber;
    private String Status;

    public Books(){
        
    }
    
    public Books(int bookId, String bookName, String Author, int rackNumber, String Status) {
        this.bookId = bookId;
        this.bookName = bookName;
        this.Author = Author;
        this.rackNumber = rackNumber;
        this.Status = Status;
    }

    public Books(String bookName, String Author, int rackNumber, String Status) {
        this.bookName = bookName;
        this.Author = Author;
        this.rackNumber = rackNumber;
        this.Status = Status;
    }

    public int getBookId() {
        return bookId;
    }

    public void setBookId(int bookId) {
        this.bookId = bookId;
    }

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }
        
    public String getAuthor() {
        return Author;
    }

    public void setAuthor(String Author) {
        this.Author = Author;
    }

    public int getRackNumber() {
        return rackNumber;
    }

    public void setRackNumber(int rackNumber) {
        this.rackNumber = rackNumber;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String Status) {
        this.Status = Status;
    }

    @Override
    public String toString() {
        return "Books{" + "bookId=" + bookId + ", bookName=" + bookName + ", Author=" + Author + ", rackNumber=" + rackNumber + ", Status=" + Status + '}';
    }
    
    
    
    
}
