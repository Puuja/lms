
package org.personal.LibraryManagement.model;


public class User {

    private int userId;
    private String userName;
    private String contact;
    private String Address;

    public User() {

    }

    public User(int userId, String userName, String contact, String Address) {
        this.userId = userId;
        this.userName = userName;
        this.contact = contact;
        this.Address = Address;
    }

    public User(String userName, String contact, String Address) {
        this.userName = userName;
        this.contact = contact;
        this.Address = Address;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String Address) {
        this.Address = Address;
    }

    @Override
    public String toString() {
        return "User{" + "userId=" + userId + ", userName=" + userName + ", contact=" + contact + ", Address=" + Address + '}';
    }
}
