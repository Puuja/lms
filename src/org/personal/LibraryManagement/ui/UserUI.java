package org.personal.LibraryManagement.ui;

import java.awt.Component;
import java.awt.HeadlessException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import org.personal.LibraryManagement.dao.UserDao;
import org.personal.LibraryManagement.dao.impl.UserDaoImpl;
import org.personal.LibraryManagement.model.User;



public final class UserUI extends javax.swing.JFrame {

    private final UserDao userDao = new UserDaoImpl();

    public UserUI() throws Exception {
        initComponents();
        Show_DatabaseTable_Into_dataTable();
        this.setLocationRelativeTo(null);
        loadData();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        userTitleLabel = new javax.swing.JLabel();
        userNameLabel = new javax.swing.JLabel();
        userNameTextField = new javax.swing.JTextField();
        userContactLabel = new javax.swing.JLabel();
        contactTextField = new javax.swing.JTextField();
        userAddressLabel = new javax.swing.JLabel();
        addressTextField = new javax.swing.JTextField();
        ExitButton = new javax.swing.JButton();
        userNameLabel1 = new javax.swing.JLabel();
        userNameTextField1 = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        dataTable = new javax.swing.JTable();
        jPanel3 = new javax.swing.JPanel();
        userSaveButton = new javax.swing.JButton();
        userEditButtom = new javax.swing.JButton();
        userDeleteButton = new javax.swing.JButton();
        UsersBackButton = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("User Dashboard");

        jPanel2.setBackground(new java.awt.Color(0, 0, 153));

        jPanel1.setBackground(java.awt.SystemColor.activeCaption);
        jPanel1.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        userTitleLabel.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        userTitleLabel.setForeground(new java.awt.Color(0, 0, 153));
        userTitleLabel.setText("USER DASHBOARD");

        userNameLabel.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        userNameLabel.setText("User Name");

        userNameTextField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                userNameTextFieldActionPerformed(evt);
            }
        });

        userContactLabel.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        userContactLabel.setText("Contact");

        contactTextField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                contactTextFieldActionPerformed(evt);
            }
        });

        userAddressLabel.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        userAddressLabel.setText("Address");

        ExitButton.setBackground(java.awt.Color.red);
        ExitButton.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        ExitButton.setText("EXIT");
        ExitButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ExitButtonActionPerformed(evt);
            }
        });

        userNameLabel1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        userNameLabel1.setText("User Id");

        userNameTextField1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                userNameTextField1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(24, 24, 24)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(userNameLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(userContactLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(userNameLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(userAddressLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(contactTextField, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(userNameTextField)
                            .addComponent(addressTextField)
                            .addComponent(userNameTextField1, javax.swing.GroupLayout.Alignment.LEADING)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(82, 82, 82)
                        .addComponent(userTitleLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 232, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(ExitButton)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(7, 7, 7)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(userTitleLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(ExitButton))
                .addGap(12, 12, 12)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(userNameTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(userNameLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(userContactLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(contactTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(userAddressLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(addressTextField, javax.swing.GroupLayout.DEFAULT_SIZE, 33, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(userNameLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 28, Short.MAX_VALUE)
                    .addComponent(userNameTextField1))
                .addContainerGap(31, Short.MAX_VALUE))
        );

        dataTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "User Name", "Contact Number", "Address"
            }
        ));
        jScrollPane1.setViewportView(dataTable);

        jPanel3.setBackground(java.awt.SystemColor.activeCaption);

        userSaveButton.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        userSaveButton.setText("Save");
        userSaveButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                userSaveButtonActionPerformed(evt);
            }
        });

        userEditButtom.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        userEditButtom.setText("Edit");
        userEditButtom.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                userEditButtomActionPerformed(evt);
            }
        });

        userDeleteButton.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        userDeleteButton.setText("Delete");
        userDeleteButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                userDeleteButtonActionPerformed(evt);
            }
        });

        UsersBackButton.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        UsersBackButton.setText("Reserve Book");
        UsersBackButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                UsersBackButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(userSaveButton)
                .addGap(18, 18, 18)
                .addComponent(userEditButtom)
                .addGap(18, 18, 18)
                .addComponent(userDeleteButton)
                .addGap(18, 18, 18)
                .addComponent(UsersBackButton)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(userSaveButton)
                    .addComponent(userEditButtom)
                    .addComponent(userDeleteButton)
                    .addComponent(UsersBackButton))
                .addContainerGap(14, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 181, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(20, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    public void Show_DatabaseTable_Into_dataTable() throws Exception {
        List<User> userList = userDao.findAll();
        DefaultTableModel defaultTablemodel = (DefaultTableModel) dataTable.getModel();
        Object[] row = new Object[6];
        for (int i = 0; i < userList.size(); i++) {
            row[0] = userList.get(i).getUserId();
            row[1] = userList.get(i).getUserName();
            row[2] = userList.get(i).getContact();
            row[3] = userList.get(i).getAddress();
            defaultTablemodel.addRow(row);
        }
    }

    private void showMessageDialog(String message) throws HeadlessException {
        JOptionPane.showMessageDialog(null, message);
    }

    private void resetTextField() {
        userNameTextField1.setText("");
        userNameTextField.setText("");
        contactTextField.setText("");
        addressTextField.setText("");
    }

    public void loadData() throws Exception {
        String columns[] = {"ID", "UserName", "Contact Number", "Address"};
        DefaultTableModel tableModel = new DefaultTableModel(columns, 0);
        dataTable.setModel(tableModel);
        try {
            for (User u : userDao.findAll()) {
                Object[] data = {u.getUserId(), u.getUserName(), u.getContact(), u.getAddress()};
                tableModel.addRow(data);
            }
        } catch (Exception ex) {
            showMessageDialog(ex.getMessage());
        }
    }
    private void contactTextFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_contactTextFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_contactTextFieldActionPerformed

    private void userSaveButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_userSaveButtonActionPerformed

        User user = new User(Integer.parseInt(userNameTextField1.getText()), userNameTextField.getText(), contactTextField.getText(), addressTextField.getText());
        try {
            
                userDao.save(user);
                showMessageDialog("User Added!!!");

            
            loadData();
        } catch (Exception ex) {
            showMessageDialog(ex.getMessage());
        }

        resetTextField();
    }//GEN-LAST:event_userSaveButtonActionPerformed

    private void userEditButtomActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_userEditButtomActionPerformed

        int i = dataTable.getSelectedRow();
        TableModel tableModel = dataTable.getModel();
        editId = (int) tableModel.getValueAt(i, 0);
        userNameTextField.setText(tableModel.getValueAt(i, 1).toString());
        contactTextField.setText(tableModel.getValueAt(i, 2).toString());
        addressTextField.setText(tableModel.getValueAt(i, 3).toString());
    }//GEN-LAST:event_userEditButtomActionPerformed

    private void dataTableMouseClicked(java.awt.event.MouseEvent evt) {

        int i = dataTable.getSelectedRow();
        TableModel tableModel = dataTable.getModel();
        userNameTextField.setText(tableModel.getValueAt(i, 1).toString());
        contactTextField.setText(tableModel.getValueAt(i, 2).toString());
        addressTextField.setText(tableModel.getValueAt(i, 3).toString());

    }


    private void userDeleteButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_userDeleteButtonActionPerformed
        int i = dataTable.getSelectedRow();
        int id = Integer.parseInt(dataTable.getValueAt(i, 0).toString());
        try {
            userDao.remove(id);
            DefaultTableModel defaultTableModel = (DefaultTableModel) dataTable.getModel();
            defaultTableModel.setRowCount(0);
            loadData();
            showMessageDialog("User Deleted!!!");
        } catch (Exception ex) {
            Logger.getLogger(UserUI.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_userDeleteButtonActionPerformed

    private void UsersBackButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_UsersBackButtonActionPerformed
                UserBooksUI userbooksUI;
        try {
        userbooksUI = new UserBooksUI();
        userbooksUI.setVisible(true);
        } catch (Exception ex) {
        Logger.getLogger(UserBooksUI.class.getName()).log(Level.SEVERE, null, ex);
        }
    this.setVisible(false);
        /*Login userbooksUI;
        try {
        userbooksUI = new Login();
        userbooksUI.setVisible(true);
        } catch (Exception ex) {
        Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.setVisible(false);*/

     }//GEN-LAST:event_UsersBackButtonActionPerformed

    private void ExitButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ExitButtonActionPerformed
        Component frame = null;
        if (JOptionPane.showConfirmDialog(frame, "Confirm if you want to exit", "Library Management System",
                JOptionPane.YES_NO_OPTION) == JOptionPane.YES_NO_OPTION) {
            System.exit(0);
        }
    }//GEN-LAST:event_ExitButtonActionPerformed

    private void userNameTextFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_userNameTextFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_userNameTextFieldActionPerformed

    private void userNameTextField1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_userNameTextField1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_userNameTextField1ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(UserUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(UserUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(UserUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(UserUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    new UserUI().setVisible(true);
                } catch (Exception ex) {
                    Logger.getLogger(UserUI.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
    }

    private int editId;
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton ExitButton;
    private javax.swing.JButton UsersBackButton;
    private javax.swing.JTextField addressTextField;
    private javax.swing.JTextField contactTextField;
    private javax.swing.JTable dataTable;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel userAddressLabel;
    private javax.swing.JLabel userContactLabel;
    private javax.swing.JButton userDeleteButton;
    private javax.swing.JButton userEditButtom;
    private javax.swing.JLabel userNameLabel;
    private javax.swing.JLabel userNameLabel1;
    private javax.swing.JTextField userNameTextField;
    private javax.swing.JTextField userNameTextField1;
    private javax.swing.JButton userSaveButton;
    private javax.swing.JLabel userTitleLabel;
    // End of variables declaration//GEN-END:variables
}
