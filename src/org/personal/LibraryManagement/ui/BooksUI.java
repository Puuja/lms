
package org.personal.LibraryManagement.ui;

import java.awt.Component;
import java.awt.HeadlessException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import org.personal.LibraryManagement.dao.BooksDao;
import org.personal.LibraryManagement.dao.impl.BooksDaoImpl;
import org.personal.LibraryManagement.model.Books;


public final class BooksUI extends javax.swing.JFrame {

    BooksDao booksDao = new BooksDaoImpl();

    public BooksUI() throws Exception {
        initComponents();
        this.setLocationRelativeTo(null);
        Show_DatabaseTable_Into_JTable();

    }

    private void showMessageDialog(String message) throws HeadlessException {
        JOptionPane.showMessageDialog(null, message);
    }

    private void resetTextField() {

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel5 = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        booksTitleLabel = new javax.swing.JLabel();
        bookNameLabel = new javax.swing.JLabel();
        authorLabel = new javax.swing.JLabel();
        rackNumberLabel = new javax.swing.JLabel();
        statusLabel = new javax.swing.JLabel();
        bookNameTextField = new javax.swing.JTextField();
        authorTextField = new javax.swing.JTextField();
        rackNumberTextField = new javax.swing.JTextField();
        statusTextField = new javax.swing.JTextField();
        ExitButton = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();
        BackBooksButton = new javax.swing.JButton();
        bookNameTextField1 = new javax.swing.JTextField();
        bookNameLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jPanel1 = new javax.swing.JPanel();
        booksSaveButton = new javax.swing.JButton();
        booksDeleteButton = new javax.swing.JButton();
        updateSaveButton = new javax.swing.JButton();
        resetTextField = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Book Dashboard");

        jPanel5.setBackground(new java.awt.Color(0, 51, 153));

        jPanel4.setBackground(java.awt.SystemColor.activeCaption);
        jPanel4.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        booksTitleLabel.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        booksTitleLabel.setForeground(new java.awt.Color(0, 0, 153));
        booksTitleLabel.setText("BOOKS DASHBOARD");

        bookNameLabel.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        bookNameLabel.setText("Book Name");

        authorLabel.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        authorLabel.setText("Author Name");

        rackNumberLabel.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        rackNumberLabel.setText("Rack Number");

        statusLabel.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        statusLabel.setText("Status");

        bookNameTextField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bookNameTextFieldActionPerformed(evt);
            }
        });

        rackNumberTextField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rackNumberTextFieldActionPerformed(evt);
            }
        });

        ExitButton.setBackground(java.awt.Color.red);
        ExitButton.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        ExitButton.setText("EXIT");
        ExitButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ExitButtonActionPerformed(evt);
            }
        });

        jButton1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jButton1.setText("Add User");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        BackBooksButton.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        BackBooksButton.setText("Records");
        BackBooksButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BackBooksButtonActionPerformed(evt);
            }
        });

        bookNameTextField1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bookNameTextField1ActionPerformed(evt);
            }
        });

        bookNameLabel1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        bookNameLabel1.setText("Book Id");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGap(77, 77, 77)
                        .addComponent(booksTitleLabel)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 111, Short.MAX_VALUE)
                        .addComponent(ExitButton))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(bookNameLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(authorLabel, javax.swing.GroupLayout.DEFAULT_SIZE, 102, Short.MAX_VALUE)
                                .addComponent(rackNumberLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(statusLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addComponent(bookNameLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(bookNameTextField, javax.swing.GroupLayout.DEFAULT_SIZE, 277, Short.MAX_VALUE)
                            .addComponent(rackNumberTextField)
                            .addComponent(statusTextField)
                            .addComponent(authorTextField)
                            .addComponent(bookNameTextField1, javax.swing.GroupLayout.DEFAULT_SIZE, 277, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jButton1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(BackBooksButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(booksTitleLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(bookNameTextField1, javax.swing.GroupLayout.DEFAULT_SIZE, 37, Short.MAX_VALUE)
                            .addComponent(bookNameLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addComponent(ExitButton))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(bookNameLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(bookNameTextField)
                    .addComponent(jButton1, javax.swing.GroupLayout.DEFAULT_SIZE, 35, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(authorLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(authorTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(BackBooksButton, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(rackNumberLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(rackNumberTextField, javax.swing.GroupLayout.DEFAULT_SIZE, 35, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(statusLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(statusTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10))
        );

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Book_Id", "Book_Name", "Author", "Rack_Number", "Status"
            }
        ));
        jTable1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable1MouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(jTable1);

        jPanel1.setBackground(java.awt.SystemColor.activeCaption);

        booksSaveButton.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        booksSaveButton.setText("Save");
        booksSaveButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                booksSaveButtonActionPerformed(evt);
            }
        });

        booksDeleteButton.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        booksDeleteButton.setText("Delete");
        booksDeleteButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                booksDeleteButtonActionPerformed(evt);
            }
        });

        updateSaveButton.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        updateSaveButton.setText("Update");
        updateSaveButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                updateSaveButtonActionPerformed(evt);
            }
        });

        resetTextField.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        resetTextField.setText("RESET");
        resetTextField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                resetTextFieldActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(booksSaveButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(booksDeleteButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(updateSaveButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(resetTextField)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE, false)
                    .addComponent(booksDeleteButton)
                    .addComponent(booksSaveButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(updateSaveButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(resetTextField))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGap(92, 92, 92)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 512, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 38, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 191, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(38, 38, 38))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    public void Show_DatabaseTable_Into_JTable() throws Exception {
        List<Books> bookList = booksDao.findAll();
        DefaultTableModel defaultTablemodel = (DefaultTableModel) jTable1.getModel();
        Object[] row = new Object[6];
        for (int i = 0; i < bookList.size(); i++) {
            row[0] = bookList.get(i).getBookId();
            row[1] = bookList.get(i).getBookName();
            row[2] = bookList.get(i).getAuthor();
            row[3] = bookList.get(i).getRackNumber();
            row[4] = bookList.get(i).getStatus();

            defaultTablemodel.addRow(row);
        }
    }

    private void rackNumberTextFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rackNumberTextFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_rackNumberTextFieldActionPerformed

    private void booksDeleteButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_booksDeleteButtonActionPerformed
        int i = jTable1.getSelectedRow();
        int id = Integer.parseInt(jTable1.getValueAt(i, 0).toString());
        try {
            booksDao.remove(id);
            DefaultTableModel defaultTableModel = (DefaultTableModel) jTable1.getModel();
            defaultTableModel.setRowCount(0);
            Show_DatabaseTable_Into_JTable();
        } catch (Exception ex) {
            Logger.getLogger(BooksUI.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_booksDeleteButtonActionPerformed

    private void booksSaveButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_booksSaveButtonActionPerformed
        Books book = new Books(Integer.parseInt(bookNameTextField1.getText()), bookNameTextField.getText(), authorTextField.getText(), Integer.parseInt(rackNumberTextField.getText()), statusTextField.getText());
        try {
            booksDao.save(book);
            DefaultTableModel defaultTableModel = (DefaultTableModel) jTable1.getModel();
            defaultTableModel.setRowCount(0);
            Show_DatabaseTable_Into_JTable();
        } catch (Exception ex) {
            Logger.getLogger(BooksUI.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_booksSaveButtonActionPerformed

    private void bookNameTextFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bookNameTextFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_bookNameTextFieldActionPerformed

    private void jTable1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable1MouseClicked

        int i = jTable1.getSelectedRow();
        TableModel tableModel = jTable1.getModel();
        bookNameTextField.setText(tableModel.getValueAt(i, 1).toString());
        authorTextField.setText(tableModel.getValueAt(i, 2).toString());
        rackNumberTextField.setText(tableModel.getValueAt(i, 3).toString());
        statusTextField.setText(tableModel.getValueAt(i, 4).toString());
    }//GEN-LAST:event_jTable1MouseClicked

    private void updateSaveButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_updateSaveButtonActionPerformed
        int i = jTable1.getSelectedRow();
        int id = Integer.parseInt(jTable1.getValueAt(i, 0).toString());
        Books books = new Books(bookNameTextField.getText(), authorTextField.getText(), Integer.parseInt(rackNumberTextField.getText()), statusTextField.getText());
        try {

            booksDao.update(books, id);

            DefaultTableModel defaultTableModel = (DefaultTableModel) jTable1.getModel();
            defaultTableModel.setRowCount(0);
            Show_DatabaseTable_Into_JTable();
        } catch (Exception ex) {
            Logger.getLogger(BooksUI.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_updateSaveButtonActionPerformed

    private void BackBooksButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BackBooksButtonActionPerformed

        UserBooksUI userbooksUI;
        try {
            userbooksUI = new UserBooksUI();
            userbooksUI.setVisible(true);
        } catch (Exception ex) {
            Logger.getLogger(UserBooksUI.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.setVisible(false);
    }//GEN-LAST:event_BackBooksButtonActionPerformed

    private void resetTextFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_resetTextFieldActionPerformed
        bookNameTextField.setText("");
        authorTextField.setText("");
        rackNumberTextField.setText("");
        statusTextField.setText("");
    }//GEN-LAST:event_resetTextFieldActionPerformed

    private void ExitButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ExitButtonActionPerformed
        Component frame = null;
        if (JOptionPane.showConfirmDialog(frame, "Confirm if you want to exit", "Library Management System",
                JOptionPane.YES_NO_OPTION) == JOptionPane.YES_NO_OPTION) {
            System.exit(0);
        }
    }//GEN-LAST:event_ExitButtonActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        UserUI userUI;
        try {
            userUI = new UserUI();
            userUI.setVisible(true);
        } catch (Exception ex) {
            Logger.getLogger(UserBooksUI.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.setVisible(false);
    }//GEN-LAST:event_jButton1ActionPerformed

    private void bookNameTextField1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bookNameTextField1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_bookNameTextField1ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(BooksUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(BooksUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(BooksUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(BooksUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                try {
                    new BooksUI().setVisible(true);
                } catch (Exception ex) {
                    Logger.getLogger(BooksUI.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton BackBooksButton;
    private javax.swing.JButton ExitButton;
    private javax.swing.JLabel authorLabel;
    private javax.swing.JTextField authorTextField;
    private javax.swing.JLabel bookNameLabel;
    private javax.swing.JLabel bookNameLabel1;
    private javax.swing.JTextField bookNameTextField;
    private javax.swing.JTextField bookNameTextField1;
    private javax.swing.JButton booksDeleteButton;
    private javax.swing.JButton booksSaveButton;
    private javax.swing.JLabel booksTitleLabel;
    private javax.swing.JButton jButton1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    private javax.swing.JLabel rackNumberLabel;
    private javax.swing.JTextField rackNumberTextField;
    private javax.swing.JButton resetTextField;
    private javax.swing.JLabel statusLabel;
    private javax.swing.JTextField statusTextField;
    private javax.swing.JButton updateSaveButton;
    // End of variables declaration//GEN-END:variables
}
